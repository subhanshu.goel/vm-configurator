import React, { useState } from "react";
import logo from "./logo.svg";
import "./scss/defaults.scss";
import "./App.scss";
import { callbackify } from "util";

import { Region, RegionEnum } from "./Components/Region";
import { PlatformList } from "./Components/PlatformList";
import { PlatformEnum, platforms } from "./Components/PlatformCard";
import {
  CostItem,
  CostEstimates,
  VM_Attribute,
} from "./Components/CostEstimates";
import { InstanceList } from "./Components/InstanceList";
import { IO } from "./Components/StorageNetwork/IO";

const Header: React.FC = () => {
  return (
    <header className="header">
      <h1 className="header-heading">HVC</h1>
    </header>
  );
};

interface ConfiguratorTab {
  title: string;
}

const configuratorTabs: ConfiguratorTab[] = [
  { title: "Choose Image" },
  { title: "Choose Instance Type" },
  { title: "Choose Storage And Network" },
  { title: "Configure Security" },
  { title: "Review & Launch" },
];

const findVMCostItemIndex = (
  costItems: CostItem[],
  attr: VM_Attribute
): number => {
  const indx = costItems.findIndex((item) => item.type == attr);
  return indx;
};

function App() {
  type TabIndexType = 0 | 1 | 2 | 3;
  const [configTabIndex, setConfigTabIndex] = useState<TabIndexType>(0);
  const [costItems, setCostItems] = useState<CostItem[]>([]);
  const [instanceTab, setInstanceTab] = useState<number>(0);

  const selectPlaform = (plat: PlatformEnum, arc: string, price: number) => {
    const indx = costItems.findIndex(
      (item) => item.type == VM_Attribute.PLATFORM
    );
    const arch_name = " (" + arc.toUpperCase() + ")";
    const obj = {
      description: platforms[plat].name + arch_name,
      cost: price,
      type: VM_Attribute.PLATFORM,
    };
    if (indx == -1) {
      costItems.push(obj);
    } else {
      costItems[indx] = obj;
    }
    setCostItems([...costItems]);
  };

  const instanceCostUpdate = (
    memDescp: string,
    memPrice: number,
    cpuDescp: string,
    cpuPrice: number
  ) => {
    const updateInstanceItems = (
      attr: VM_Attribute,
      descp: string,
      price: number
    ) => {
      const indx = findVMCostItemIndex(costItems, attr);
      const obj: CostItem = {
        description: descp,
        cost: price,
        type: attr,
      };
      if (indx == -1) {
        costItems.push(obj);
      } else {
        costItems[indx] = obj;
      }
    };

    updateInstanceItems(VM_Attribute.INSTANCE_CPU, cpuDescp, cpuPrice);
    updateInstanceItems(VM_Attribute.INSTANCE_MEMORY, memDescp, memPrice);

    setCostItems([...costItems]);
  };

  const IOupdateCostItems = (ioCostItems: CostItem[]) => {
    const tempCostItemList = costItems
      .filter((item) => item.type != VM_Attribute.STORAGE)
      .filter((item) => item.type != VM_Attribute.NETWORK);
    setCostItems([...tempCostItemList, ...ioCostItems]);
  };

  const configTabTitle = configuratorTabs[configTabIndex].title;

  
  const configButtonOnClick = (index:number) => {
    setConfigTabIndex(index as TabIndexType);
  };

  const configTabElems = configuratorTabs.map((tab, index) => (
    <li
      value={index}
      className="nav-item"
      onClick={(e) => configButtonOnClick(index)}
    >
      <a className={"nav-link" + (configTabIndex == index ? " active" : "")}>
        {tab.title}
      </a>
    </li>
  ));

  let tabElem;
  switch (Number(configTabIndex)) {
    case 0:
      tabElem = <PlatformList selectPlatform={selectPlaform}></PlatformList>;
      break;
    case 1:
      tabElem = (
        <InstanceList
          instanceTab={instanceTab}
          setInstanceTab={setInstanceTab}
          instanceCostUpdate={instanceCostUpdate}
        ></InstanceList>
      );
      break;
    case 2:
      tabElem = (
        <IO
          updateCostItemsCB={IOupdateCostItems}
          instanceType={instanceTab}
        ></IO>
      );
      break;
  }

  return (
    <div className="App">
      <Header></Header>
      <div className="d-flex">
        <div>
          <h2>{configTabTitle}</h2>
          <ul className="nav">{configTabElems}</ul>
        </div>
        <Region
          selectedRegion={RegionEnum.INDIA_1}
          regionSelectCallback={() => {}}
        ></Region>
      </div>
      <div className="d-flex">
        <main>{tabElem}</main>
        <CostEstimates costItems={costItems}></CostEstimates>
      </div>
    </div>
  );
}

export default App;
