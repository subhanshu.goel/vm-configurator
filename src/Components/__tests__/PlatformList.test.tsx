import * as React from "react";
import { render, fireEvent, waitFor, cleanup } from "@testing-library/react";

import {PlatformList} from "../PlatformList";
import {platforms} from "../PlatformCard";


describe("test platform list component", () => {
    test("if list renders all platform cards", () =>{
        const fnCB =jest.fn();

        const {getByText} = render(<PlatformList selectPlatform={fnCB}></PlatformList>)

        const linux = getByText("Linux 2 Image");
        const ubuntu = getByText("Ubuntu Server 18.04 LTS");
        const redHat = getByText("Red Hat Enterprise Linux 8");
        const msWin = getByText("Microsoft Windows Server 2019 Base");
        const suse = getByText("SUSE Linux Enterprise Server");

        expect(linux).toBeInTheDocument();
        expect(ubuntu).toBeInTheDocument();
        expect(redHat).toBeInTheDocument();
        expect(msWin).toBeInTheDocument();
        expect(suse).toBeInTheDocument();

    });
})