import React from "react";
import "../App.scss";

export enum RegionEnum {
  US_EAST_1 = "us-east-1",
  US_EAST_2 = "us-east-2",
  US_WEST_1 = "us-west-1",
  INDIA_1 = "india-1",
}

interface RegionProps {
  selectedRegion: RegionEnum;
  regionSelectCallback(regionEnum: RegionEnum): any;
}

export const Region: React.FC<RegionProps> = (props: RegionProps) => {
  const selectOption = (e: any, regionEnum: RegionEnum) => {
    e.preventDefault();
    props.regionSelectCallback(regionEnum);
  };

  return (
    <nav className="region">
      <a className="">Region</a>
      <ul className="region-dropdown">
        <li>
          <a
            href="#"
            onClick={(e) => {
              selectOption(e, RegionEnum.US_EAST_1);
            }}
          >
            us-east-1
          </a>
        </li>
        <li>
          <a
            href="#"
            onClick={(e) => {
              selectOption(e, RegionEnum.US_EAST_2);
            }}
          >
            us-east-2
          </a>
        </li>
        <li>
          <a
            href="#"
            onClick={(e) => {
              selectOption(e, RegionEnum.US_WEST_1);
            }}
          >
            us-west-1
          </a>
        </li>
        <li>
          <a
            href="#"
            onClick={(e) => {
              selectOption(e, RegionEnum.INDIA_1);
            }}
          >
            india-1
          </a>
        </li>
      </ul>
    </nav>
  );
};
