import React, { useState, Fragment } from "react";

import { PlatformEnum, PlatformCard } from "./PlatformCard";
import "../scss/platform.scss";

interface PlatformProps {
  selectPlatform(plat: PlatformEnum, arc: string, price: number): any;
}

export const PlatformList: React.FC<PlatformProps> = (props: PlatformProps) => {
  return (
    <ul className="platform-list">
      <li>
        <PlatformCard
          platformType={PlatformEnum.LINUX_2_IMAGE}
          selectPlatform={props.selectPlatform}
        ></PlatformCard>
      </li>
      <li>
        <PlatformCard
          platformType={PlatformEnum.UBUNTU_18_04}
          selectPlatform={props.selectPlatform}
        ></PlatformCard>
      </li>
      <li>
        <PlatformCard
          platformType={PlatformEnum.RED_HAT_LINUX}
          selectPlatform={props.selectPlatform}
        ></PlatformCard>
      </li>
      <li>
        <PlatformCard
          platformType={PlatformEnum.MICROSOFT_WINDOWS_2019}
          selectPlatform={props.selectPlatform}
        ></PlatformCard>
      </li>
      <li>
        <PlatformCard
          platformType={PlatformEnum.SUSE_LINUX}
          selectPlatform={props.selectPlatform}
        ></PlatformCard>
      </li>
    </ul>
  );
};
