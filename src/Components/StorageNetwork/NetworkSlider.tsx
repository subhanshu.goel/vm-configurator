import React, { useState, Fragment } from "react";

import { InstanceType } from "../InstanceList";

interface NetworkProps {
  instanceType: InstanceType;
  bandwith: number;
  setBandwidth: any;
}

export const NetworkSlider: React.FC<NetworkProps> = ({
  instanceType,
  bandwith,
  setBandwidth,
}: NetworkProps) => {
  const onSliderChange = (e: any) => {
    setBandwidth(e.target.value);
  };

  let min = 512;
  let max = 1000;
  if (instanceType == InstanceType.NETWORK_OPTIMISED) {
    max = 2000;
  }

  return (
    <Fragment>
      <h2>Network Bandwidth Configuration</h2>
      <label htmlFor="traffic" className="subtitle-1"></label>
      <input
        type="range"
        list="range"
        id="traffic"
        name="traffic"
        min={min}
        max={max}
        value={bandwith}
        onChange={onSliderChange}
        role="slider"
      />
      <datalist id="range">
        <option value={min} label={min + "GB"}></option>
        <option value={max} label={max / 1000 + "TB"}></option>
      </datalist>
    </Fragment>
  );
};
