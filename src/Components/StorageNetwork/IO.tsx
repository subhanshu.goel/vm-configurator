import React, { useState, Fragment } from "react";

import { InstanceType } from "../InstanceList";
import { StorageList } from "./StorageList";
import { NetworkSlider } from "./NetworkSlider";
import { CostItem, VM_Attribute } from "../CostEstimates";

export enum StorageType {
  MAGNETIC_DISK = "Magnetic Disk",
  SSD = "SSD",
}

export enum VolumeType {
  ROOT = "Root",
  EXT = "Ext",
}

type IOPS = 100 | 600 | 1000;

export interface Storage {
  type: StorageType;
  volume: VolumeType;
  capacity: number;
  encryption: boolean;
  iops: IOPS;
  backup: boolean;
  remarks?: string;
  errorState: boolean;
}

const initialStorage: Storage = {
  type: StorageType.SSD,
  volume: VolumeType.ROOT,
  capacity: 50,
  encryption: false,
  iops: 100,
  backup: false,
  errorState: false,
};

const newStorage = (): Storage => {
  return {
    type: StorageType.SSD,
    volume: VolumeType.EXT,
    capacity: 50,
    encryption: false,
    iops: 100,
    backup: false,
    errorState: false,
  };
};

interface IOProps {
  instanceType: InstanceType;
  updateCostItemsCB(costItems: CostItem[]): any;
}

export const IO: React.FC<IOProps> = (props: IOProps) => {
  const [storageList, setStorageList] = useState<Storage[]>([initialStorage]);
  const [bandwith, setBandwidth] = useState<number>(512);

  const setStorage = (index: number, storage: Storage) => {
    storageList[index] = storage;
    setStorageList([...storageList]);
  };

  const deleteStorage = (index: number) => {
    storageList.splice(index, 1);
    setStorageList([...storageList]);
  };

  const storageElementList = storageList.map((storage, index) => (
    <StorageList
      deleteStorage={deleteStorage}
      setStorage={setStorage}
      storage={storage}
      index={index}
    ></StorageList>
  ));

  const addVolume = (e: any) => {
    setStorageList([...storageList, newStorage()]);
  };

  const onSubmit = (e: any) => {
    e.preventDefault();
    const costItems: CostItem[] = [];
    for (const storage of storageList) {
      if (storage.errorState == true) {
        //Raise error instead
        return;
      } else {
        let cost = 0;
        if (storage.volume == VolumeType.EXT) {
          if (storage.type == StorageType.SSD) {
            cost = 40;
          } else {
            cost = 20;
          }
        }
        const costItem: CostItem = {
          description:
            storage.type +
            " (" +
            storage.volume +
            ")" +
            " - " +
            storage.capacity +
            " GB",
          type: VM_Attribute.STORAGE,
          cost: cost,
        };
        costItems.push(costItem);
      }
    }
    let bandwithCostItem: CostItem = {
      description: "Bandwidth" + " - " + bandwith + " GB",
      type: VM_Attribute.NETWORK,
      cost: 5,
    };
    if (bandwith > 1000 && bandwith < 1500) {
      bandwithCostItem.cost = 10;
    } else {
      bandwithCostItem.cost = 15;
    }
    props.updateCostItemsCB([...costItems, bandwithCostItem]);
  };

  return (
    <form onSubmit={onSubmit}>
      <section>
        <ul>{storageElementList}</ul>
        <button type="button" onClick={addVolume}>
          Add Volume
        </button>
      </section>
      <section>
        <NetworkSlider
          instanceType={props.instanceType}
          bandwith={bandwith}
          setBandwidth={setBandwidth}
        ></NetworkSlider>
      </section>
      <input type="submit" value="Proceed" />
    </form>
  );
};
