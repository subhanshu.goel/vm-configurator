import * as React from "react";
import {
  render,
  fireEvent,
  waitFor,
  cleanup,
} from "@testing-library/react";

import { Storage, StorageType, VolumeType } from "../IO";
import { StorageList } from "../StorageList";

const newStorage = (): Storage => {
  return {
    type: StorageType.SSD,
    volume: VolumeType.EXT,
    capacity: 50,
    encryption: true,
    iops: 100,
    backup: false,
    errorState: false,
  };
};

afterEach(cleanup);

describe("test storage component list", () => {
  test("if all the fields are renderd", () => {
    const { getByLabelText } = render(
      <StorageList
        index={0}
        storage={newStorage()}
        setStorage={() => {}}
        deleteStorage={() => {}}
      ></StorageList>
    );

    const text: String[] = [
      "Type",
      "Volume",
      "Capacity(GB)",
      "Encryption",
      "IOPS",
      "Backup Required",
      "Remarks",
    ];

    const elemList = text.map((str) => getByLabelText(str));

    const storageType = getByLabelText("Type");
    const volumeType = getByLabelText("Volume");
    const capacity = getByLabelText("Capacity(GB)");
    const encryption = getByLabelText("Encryption");
    const iops = getByLabelText("IOPS");
    const backup = getByLabelText("Backup Required");
    const remarks = getByLabelText("Remarks");

    expect(volumeType).toHaveAttribute("value", "Ext");

    for (const elem of elemList) {
      expect(elem).toBeInTheDocument();
    }
  });

  test("if all fields have used values from props", () => {
    const { getByLabelText } = render(
      <StorageList
        index={0}
        storage={newStorage()}
        setStorage={() => {}}
        deleteStorage={() => {}}
      ></StorageList>
    );

    const storageType = getByLabelText("Type");
    const volumeType = getByLabelText("Volume");
    const capacity = getByLabelText("Capacity(GB)");
    const encryption = getByLabelText("Encryption");
    const iops = getByLabelText("IOPS");
    const backup = getByLabelText("Backup Required");
    const remarks = getByLabelText("Remarks");

    expect(volumeType).toHaveAttribute("value", "Ext");
    expect(capacity).toHaveAttribute("value", "50");
    expect(encryption).toHaveAttribute("checked", "");
    expect(iops).toHaveAttribute("value", "100");
    expect(backup).not.toHaveAttribute("checked");
    expect(remarks).toHaveAttribute("value", "");
  });

  test("if volume field trigger state update on change", () => {
    const fnCB = jest.fn();
    const { getByLabelText } = render(
      <StorageList
        index={0}
        storage={newStorage()}
        setStorage={fnCB}
        deleteStorage={() => {}}
      ></StorageList>
    );

    const storageType = getByLabelText("Type");
    const volumeType = getByLabelText("Volume");
    const remarks = getByLabelText("Remarks");

    fireEvent.change(volumeType, { target: { value: "Root" } });

    const testStorage = (newStorage().volume = VolumeType.ROOT);
    waitFor(() => expect(fnCB).toBeCalledWith(0, testStorage));
  });

  test("if storage field trigger state update on change", () => {
    const fnCB = jest.fn();
    const { getByLabelText } = render(
      <StorageList
        index={0}
        storage={newStorage()}
        setStorage={fnCB}
        deleteStorage={() => {}}
      ></StorageList>
    );

    const storageType = getByLabelText("Type");

    fireEvent.change(storageType, { target: { value: "Magnetic Disk" } });

    const testStorage = (newStorage().type = StorageType.MAGNETIC_DISK);
    waitFor(() => expect(fnCB).toBeCalledWith(0, testStorage));
  });

  test("if capacity field triggers state update on change", () => {
    const fnCB = jest.fn();
    const { getByLabelText } = render(
      <StorageList
        index={0}
        storage={newStorage()}
        setStorage={fnCB}
        deleteStorage={() => {}}
      ></StorageList>
    );

    const capacity = getByLabelText("Capacity(GB)");
    const iops = getByLabelText("IOPS");

    fireEvent.change(capacity, { target: { value: 200 } });
    const testStorage = (newStorage().capacity = 200);
    waitFor(() => expect(fnCB).toBeCalledWith(0, testStorage));
    waitFor(() => expect(iops).toHaveAttribute("value", "600"));

    fireEvent.change(capacity, { target: { value: 600 } });
    const testStorage1 = (newStorage().capacity = 600);
    waitFor(() => expect(fnCB).toBeCalledWith(0, testStorage1));
    waitFor(() => expect(iops).toHaveAttribute("value", "1000"));
  });

  test("if capacity fields for SSD shows error when value is outside of range", () => {
    const fnCB = jest.fn();
    const { getByLabelText, getByText } = render(
      <StorageList
        index={0}
        storage={newStorage()}
        setStorage={fnCB}
        deleteStorage={() => {}}
      ></StorageList>
    );

    const capacity = getByLabelText("Capacity(GB)");

    fireEvent.change(capacity, { target: { value: 2 } });
    waitFor(() =>
      expect(getByText("Minimum capacity is 20 GB")).toBeInTheDocument()
    );

    fireEvent.change(capacity, { target: { value: 276600 } });
    waitFor(() =>
      expect(getByText("Maximum capacity is 512 GB")).toBeInTheDocument()
    );
  });

  test("if capacity field for magnetic disk shows error when value is outside of range", () => {
    const testStorage = newStorage();
    testStorage.type = StorageType.MAGNETIC_DISK;

    const { getByLabelText, getByText } = render(
      <StorageList
        index={0}
        storage={testStorage}
        setStorage={() => {}}
        deleteStorage={() => {}}
      ></StorageList>
    );

    const capacity = getByLabelText("Capacity(GB)");

    fireEvent.change(capacity, { target: { value: 2 } });
    waitFor(() =>
      expect(getByText("Minimum capacity is 40 GB")).toBeInTheDocument()
    );

    fireEvent.change(capacity, { target: { value: 276600 } });
    waitFor(() =>
      expect(getByText("Maximum capacity is 2 TB")).toBeInTheDocument()
    );
  });

  test("if encryption field triggers state update on change", () => {
    const fnCB = jest.fn();
    const { getByLabelText } = render(
      <StorageList
        index={0}
        storage={newStorage()}
        setStorage={fnCB}
        deleteStorage={() => {}}
      ></StorageList>
    );

    const encryption = getByLabelText("Encryption");

    fireEvent.change(encryption, { target: { value: false } });
    const testStorage = (newStorage().encryption = false);
    waitFor(() => expect(fnCB).toBeCalledWith(0, testStorage));
  });

  test("if backup field triggers state update on change", () => {
    const fnCB = jest.fn();
    const { getByLabelText } = render(
      <StorageList
        index={0}
        storage={newStorage()}
        setStorage={fnCB}
        deleteStorage={() => {}}
      ></StorageList>
    );

    const backup = getByLabelText("Backup Required");

    fireEvent.change(backup, { target: { value: true } });
    const testStorage = (newStorage().encryption = true);
    waitFor(() => expect(fnCB).toBeCalledWith(0, testStorage));
  });
});
