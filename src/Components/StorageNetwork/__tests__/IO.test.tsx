import * as React from "react";
import { render, fireEvent, waitFor, cleanup } from "@testing-library/react";

import { InstanceType } from "../../InstanceList";
import {IO} from "../IO";


describe("test network and storage component", () => {
    test("if component renders network element", ()=> {
        const fnCB = jest.fn();
        const {getByText} = render(<IO instanceType={InstanceType.CPU_OPTIMISED} updateCostItemsCB={fnCB}></IO>)

        const networkHeading = getByText("Network Bandwidth Configuration");

        expect(networkHeading).toBeInTheDocument();
    });

    test("if component renders storage elements", ()=> {
        const fnCB = jest.fn();
        const {getByLabelText} = render(<IO instanceType={InstanceType.CPU_OPTIMISED} updateCostItemsCB={fnCB}></IO>)

        const text: String[] = [
        "Type",
        "Volume",
        "Capacity(GB)",
        "Encryption",
        "IOPS",
        "Backup Required",
        "Remarks",
        ];

        const elemList = text.map((str) => getByLabelText(str));
        for (const elem of elemList) {
        expect(elem).toBeInTheDocument();
        }
    });
})