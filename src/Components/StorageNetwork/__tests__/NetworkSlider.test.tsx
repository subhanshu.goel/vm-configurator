import * as React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";

import { InstanceType } from "../../InstanceList";
import { NetworkSlider } from "../NetworkSlider";

describe("test network slider", () => {
  test("if slider renders", () => {
    const { getByRole } = render(
      <NetworkSlider
        instanceType={InstanceType.CPU_OPTIMISED}
        bandwith={1000}
        setBandwidth={() => {}}
      ></NetworkSlider>
    );

    const slider = getByRole("slider");

    expect(slider).toBeInTheDocument();
  });

  test("if slider runs callback on change", () => {
    const fnCB = jest.fn();
    const { getByRole } = render(
      <NetworkSlider
        instanceType={InstanceType.CPU_OPTIMISED}
        bandwith={1000}
        setBandwidth={fnCB}
      ></NetworkSlider>
    );

    const slider = getByRole("slider");

    fireEvent.change(slider, { target: { value: 1000 } });
    waitFor(() => expect(fnCB).toBeCalled());
  });

  test("if slider runs callback with given value", () => {
    const fnCB = jest.fn();
    const { getByRole } = render(
      <NetworkSlider
        instanceType={InstanceType.CPU_OPTIMISED}
        bandwith={1000}
        setBandwidth={fnCB}
      ></NetworkSlider>
    );

    const slider = getByRole("slider");

    fireEvent.change(slider, { target: { value: 1000 } });
    waitFor(() => expect(fnCB).toBeCalledWith(1000));
  });
});
