import React, { useState, Fragment } from "react";

import { Storage, StorageType, VolumeType } from "./IO";

interface StorageProps {
  storage: Storage;
  index: number;
  setStorage(index: number, storage: Storage): any;
  deleteStorage(index: number): any;
}

export const StorageList: React.FC<StorageProps> = ({
  index,
  storage,
  setStorage,
  deleteStorage,
}: StorageProps) => {
  const [capacityError, setCapacityError] = useState<string>("");
  const [showStorageStr, setShowStorageStr] = useState<boolean>(true);

  const storageOnClick = (e: any) => {
    storage.type = e.target.value;
    setStorage(index, storage);
    setShowStorageStr(false);
  };

  const deleteStorageOnClick = (e: any) => {
    storage.type = e.target.value;
    deleteStorage(index);
  };

  const validateStorageCapacity = () => {
    if (storage.type == StorageType.MAGNETIC_DISK) {
      if (storage.capacity < 40) {
        setCapacityError("Minimum capacity is 40 GB");
        storage.errorState = true;
      } else if (storage.capacity > 2000) {
        setCapacityError("Maximum capacity is 2 TB");
        storage.errorState = true;
      } else {
        storage.errorState = false;
        setCapacityError("");
      }
    } else if (storage.type == StorageType.SSD) {
      if (storage.capacity < 20) {
        setCapacityError("Minimum capacity is 20 GB");
        storage.errorState = true;
      } else if (storage.capacity > 512) {
        setCapacityError("Maximum capacity is 512 GB");
        storage.errorState = true;
      } else {
        storage.errorState = false;
        setCapacityError("");
      }
    }
  };

  const capacityOnChange = (e: any) => {
    storage.capacity = e.target.value;

    if (storage.capacity < 100) {
      storage.iops = 100;
    } else if (storage.capacity >= 100 && storage.capacity <= 500) {
      storage.iops = 600;
    } else {
      storage.iops = 1000;
    }
    validateStorageCapacity();
    setStorage(index, storage);
  };

  const encryptOnChange = (e: any) => {
    storage.encryption = e.target.checked;
    setStorage(index, storage);
  };

  const backupOnChange = (e: any) => {
    storage.backup = e.target.checked;
    setStorage(index, storage);
  };

  const remarksOnChange = (e: any) => {
    storage.remarks = e.target.value;
    setStorage(index, storage);
  };

  return (
    <li>
      <label htmlFor={"storageType" + index}>Type</label>
      <Fragment>
        <select
          name="storageType"
          id={"storageType" + index}
          onChange={storageOnClick}
        >
          {showStorageStr ? <option value="null">Storage</option> : undefined},
          <option value={StorageType.MAGNETIC_DISK}>
            {StorageType.MAGNETIC_DISK}
          </option>
          ,<option value={StorageType.SSD}>{StorageType.SSD}</option>,
        </select>
        {/*instead of datalist, select can be used */}
        {/*<datalist id="storageTypeName" ></datalist>*/}
      </Fragment>
      <label htmlFor={"volume" + index}>Volume</label>
      <input
        type="text"
        name="volume"
        id={"volume" + index}
        value={storage.volume}
        disabled
      ></input>
      <label htmlFor={"capacity" + index}>Capacity(GB)</label>
      <input
        type="number"
        name="capacity"
        id={"capacity" + index}
        value={storage.capacity}
        onChange={capacityOnChange}
      ></input>
      <span style={{ color: "red" }}>{capacityError}</span>
      <label htmlFor={"encryption" + index}>Encryption</label>
      <input
        type="checkbox"
        name="encryption"
        id={"encryption" + index}
        checked={storage.encryption}
        onChange={encryptOnChange}
      ></input>
      <label htmlFor={"iops" + index}>IOPS</label>
      <input
        type="number"
        name="iops"
        id={"iops" + index}
        value={storage.iops}
        disabled
      ></input>
      <label htmlFor={"backup" + index}>Backup Required</label>
      <input
        type="checkbox"
        name="backup"
        id={"backup" + index}
        checked={storage.backup}
        onChange={backupOnChange}
      ></input>
      <label htmlFor={"remarks" + index}>Remarks</label>
      <input
        type="text"
        name="remarks"
        id={"remarks" + index}
        value={storage.remarks}
        onChange={remarksOnChange}
      ></input>
      {index != 0 ? (
        <button type="button" onClick={deleteStorageOnClick}>
          remove
        </button>
      ) : undefined}
    </li>
  );
};
