import React, { useState, Fragment } from "react";

import {InstanceType, instances} from "./InstanceList";

interface InstanceOptionsProp {
  instanceType: InstanceType;
  memoryVal: number;
  setMemory: any;
  cpuCoreVal: number;
  setCPU: any;
  instanceCostUpdate(
    memory: string,
    memPrice: number,
    cpu: string,
    cpuPrice: number
  ): any;
}

export const InstanceOption: React.FC<InstanceOptionsProp> = (
  props: InstanceOptionsProp
) => {
  const { memoryVal, setMemory, cpuCoreVal, setCPU } = props;

  const memOptions = Array.from(
    instances[props.instanceType].memoryOptions
  ).sort();
  const cpuOptions = Array.from(
    instances[props.instanceType].cpuOptions
  ).sort();

  const onMemoryBtnClick = (e: any) => {
    e.preventDefault();
    setMemory(e.target.value);
  };

  const onCpuBtnClick = (e: any) => {
    e.preventDefault();
    setCPU(e.target.value);
  };

  const onSubmit = (e: any) => {
    e.preventDefault();
    if (memoryVal == -1 || cpuCoreVal == -1) {
      //raise error
    } else {
      let memPrice = 0;
      if (memoryVal == 32000) {
        memPrice = 20;
      } else if (memoryVal == 64000) {
        memPrice = 40;
      }
      const memDescp =
        (memoryVal >= 1000 ? memoryVal / 1000 + " GB" : memoryVal + " MB") +
        " (" +
        InstanceType[props.instanceType].toLowerCase().replaceAll("_", " ") +
        ")";

      let cpuPrice = 0;
      if (cpuCoreVal == 8) {
        cpuPrice = 20;
      } else if (cpuCoreVal == 16) {
        cpuPrice = 40;
      }
      const cpuDescp =
        (cpuCoreVal == 1 ? cpuCoreVal + " core" : cpuCoreVal + " cores") +
        " (" +
        InstanceType[props.instanceType].toLowerCase().replaceAll("_", " ") +
        ")";
      props.instanceCostUpdate(memDescp, memPrice, cpuDescp, cpuPrice);
    }
  };

  const memOptElems = memOptions.map((option) => {
    const text = option >= 1000 ? option / 1000 + " GB" : option + " MB";
    return (
      <li>
        <button name="memory" value={option} onClick={onMemoryBtnClick}>
          {text}
        </button>
      </li>
    );
  });

  const cpuOptElems = cpuOptions.map((option) => {
    const text = option == 1 ? option + " core" : option + " cores";
    return (
      <li>
        <button name="cpu" value={option} onClick={onCpuBtnClick}>
          {text}
        </button>
      </li>
    );
  });

  return (
    <form className="flex" onSubmit={onSubmit}>
      <a className="">Memory</a>
      <ul className="dropdown">
        <a>{memOptElems}</a>
      </ul>
      <a className="">CPU Cores</a>
      <ul className="dropdown">
        <a>{cpuOptElems}</a>
      </ul>
      <input type="submit" value="Proceed"></input>
    </form>
  );
};