import { O_TRUNC } from "constants";
import { type } from "os";
import { cpuUsage } from "process";
import React, { useState, Fragment } from "react";

import { InstanceOption } from "./InstanceOption";

enum Memory {
  MB_256 = 256,
  MB_512 = 512,
  GB_1 = 1000,
  GB_2 = 2000,
  GB_4 = 4000,
  GB_16 = 16000,
  GB_32 = 32000,
  GB_64 = 64000,
}

enum CPU {
  C_1 = 1,
  C_2 = 2,
  C_4 = 4,
  C_8 = 8,
  C_16 = 16,
}

interface Instance {
  name: string;
  memoryOptions: Set<Memory>;
  cpuOptions: Set<CPU>;
}

export enum InstanceType {
  GENERAL_PURPOSE = 0,
  CPU_OPTIMISED,
  STORAGE_OPTIMISED,
  NETWORK_OPTIMISED,
}

export const instances: Instance[] = [
  {
    name: "General Purpose",
    memoryOptions: new Set([
      Memory.MB_256,
      Memory.MB_512,
      Memory.GB_1,
      Memory.GB_2,
      Memory.GB_4,
    ]),
    cpuOptions: new Set([CPU.C_1, CPU.C_2, CPU.C_4]),
  },
  {
    name: "CPU optimised",
    memoryOptions: new Set([Memory.GB_16, Memory.GB_32, Memory.GB_64]),
    cpuOptions: new Set([CPU.C_1, CPU.C_2, CPU.C_8, CPU.C_16]),
  },
  {
    name: "Storage Optimised",
    memoryOptions: new Set([Memory.GB_16, Memory.GB_32, Memory.GB_64]),
    cpuOptions: new Set([CPU.C_1, CPU.C_8, CPU.C_16]),
  },
  {
    name: "Network Optimised",
    memoryOptions: new Set([
      Memory.MB_256,
      Memory.MB_512,
      Memory.GB_1,
      Memory.GB_2,
      Memory.GB_4,
      Memory.GB_16,
      Memory.GB_32,
      Memory.GB_64,
    ]),
    cpuOptions: new Set([CPU.C_1, CPU.C_2, CPU.C_4, CPU.C_8, CPU.C_16]),
  },
];

interface InstanceListProp {
  instanceTab: number;
  setInstanceTab: any;
  instanceCostUpdate(
    memory: string,
    memPrice: number,
    cpu: string,
    cpuPrice: number
  ): any;
}

export const InstanceList: React.FC<InstanceListProp> = ({
  instanceCostUpdate,
  setInstanceTab,
  instanceTab,
}: InstanceListProp) => {
  const [memoryVal, setMemory] = useState<number>(-1);
  const [cpuCoreVal, setCPU] = useState<number>(-1);

  const instanceTypeButtons = instances.map((inst, index: number) => (
    <li>
      <button
        onClick={(e: any) => {
          e.preventDefault();
          setMemory(-1);
          setCPU(-1);
          setInstanceTab(index);
        }}
      >
        {inst.name}
      </button>
    </li>
  ));

  //const instanceOptionsElems = Object.values(InstanceType).filter((value) => !(typeof value == 'string')).map((val) => <InstanceOptions instanceType={val as number}></InstanceOptions>)

  return (
    <Fragment>
      <nav>
        <ul>{instanceTypeButtons}</ul>
      </nav>
      <InstanceOption
        instanceType={instanceTab}
        instanceCostUpdate={instanceCostUpdate}
        memoryVal={memoryVal}
        setMemory={setMemory}
        cpuCoreVal={cpuCoreVal}
        setCPU={setCPU}
      ></InstanceOption>
    </Fragment>
  );
};
