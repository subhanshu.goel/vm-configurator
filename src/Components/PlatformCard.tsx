import React, { useState, Fragment } from "react";

import "../scss/platform.scss";

export interface Platform {
  name: string;
  description: string;
  price: number;
}

export enum PlatformEnum {
  LINUX_2_IMAGE = 0,
  UBUNTU_18_04,
  RED_HAT_LINUX,
  MICROSOFT_WINDOWS_2019,
  SUSE_LINUX,
}

export const platforms: Platform[] = [
  {
    name: "Linux 2 Image",
    description:
      "Linux 2 comes with 5 years of support. It provides Linux kernel 4.14 tuned for optimal performance",
    price: 243.61,
  },
  {
    name: "Ubuntu Server 18.04 LTS",
    description:
      "Linux 2 comes with 5 years of support. It provides Linux kernel 4.14 tuned for optimal performance",
    price: 243.61,
  },
  {
    name: "Red Hat Enterprise Linux 8",
    description:
      "Linux 2 comes with 5 years of support. It provides Linux kernel 4.14 tuned for optimal performance",
    price: 300,
  },
  {
    name: "Microsoft Windows Server 2019 Base",
    description:
      "Linux 2 comes with 5 years of support. It provides Linux kernel 4.14 tuned for optimal performance",
    price: 338.77,
  },
  {
    name: "SUSE Linux Enterprise Server",
    description:
      "Linux 2 comes with 5 years of support. It provides Linux kernel 4.14 tuned for optimal performance",
    price: 200.22,
  },
];

interface PlatformCardProp {
  platformType: PlatformEnum;
  selectPlatform(plat: PlatformEnum, arc: string, price: number): any;
}

export const PlatformCard: React.FC<PlatformCardProp> = (
  props: PlatformCardProp
) => {
  const [arch, setArch] = useState("x86");

  const onArchChange = (event: any) => {
    setArch(event.target.value);
  };

  const platform: Platform = platforms[Number(props.platformType)];

  let secondRadioBtn = null;

  const onSubmit = (e: any) => {
    e.preventDefault();
    props.selectPlatform(props.platformType, arch, platform.price);
  };

  if (props.platformType != PlatformEnum.MICROSOFT_WINDOWS_2019) {
    secondRadioBtn = (
      <Fragment>
        <input
          type="radio"
          id="arm"
          name="arch"
          value="ARM"
          onChange={onArchChange}
          defaultChecked={arch == "arm"}
        />
        <label htmlFor="arm">64-bit (ARM)</label>
        <br />
      </Fragment>
    );
  }

  return (
    <article className="platform-card">
      <img src="" alt="Platform's image"></img>
      <section>
        <h3>{platform.name}</h3>
        <p>{platform.description}</p>
      </section>
      <form onSubmit={onSubmit}>
        <input
          type="radio"
          id="x86"
          name="arch"
          value="x86"
          onChange={onArchChange}
          defaultChecked={arch == "x86"}
        />
        <label htmlFor="x86">64-bit (x86)</label>
        <br />
        {secondRadioBtn}
        <input type="submit" value="Select"></input>
      </form>
    </article>
  );
};
