import React, { useMemo, useState } from "react";

import "../scss/platform.scss";
import "../scss/cost_card.scss";

export enum VM_Attribute {
  PLATFORM = 0,
  INSTANCE_MEMORY,
  INSTANCE_CPU,
  STORAGE,
  NETWORK,
  SECURITY,
}

export interface CostItem {
  description: string;
  cost: number;
  type: VM_Attribute;
}

interface CostEstimateProp {
  costItems: CostItem[];
}

export const CostEstimates: React.FC<CostEstimateProp> = (
  props: CostEstimateProp
) => {
  const itemsElem = props.costItems.map((item) => (
    <li className="cost-card--listing">
      <div>{item.description}</div>
      <div>{item.cost}</div>
    </li>
  ));

  const totalSum = useMemo(
    () =>
      props.costItems
        .map((item) => item.cost)
        .reduce((acc, curr) => acc + curr, 0),
    [props.costItems]
  );

  return (
    <aside className="cost-card">
      <h3>Cost Estimates</h3>
      <ul className="cost-card--list"> {itemsElem}</ul>
      <div className="text-align-right">${totalSum}/mo</div>
    </aside>
  );
};
