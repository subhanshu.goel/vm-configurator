import * as React from "react";
import {
  render,
  fireEvent,
  cleanup,
  getByRole,
  getByTestId,
  getByText,
} from "@testing-library/react";

import { Region, RegionEnum } from "../Components/Region";

const regionNames = ["us-east-1", "us-east-2", "us-west-1", "india-1"];

describe("test region component", () => {
  test("if component renders all regions", () => {
    const regionSelect = jest.fn();
    const { getByText } = render(
      <Region
        selectedRegion={RegionEnum.INDIA_1}
        regionSelectCallback={regionSelect}
      />
    );

    for (const val of regionNames) {
      const elem = getByText(val);

      expect(elem).toBeInTheDocument();
    }
  });

  test("if callback runs on selecting a region", () => {
    for (const val of regionNames) {
      const regionSelect = jest.fn();
      const { getByText, unmount } = render(
        <Region
          selectedRegion={RegionEnum.INDIA_1}
          regionSelectCallback={regionSelect}
        />
      );
      const elem = getByText(val);

      fireEvent.click(elem);
      expect(regionSelect).toBeCalled();
      expect(regionSelect).toBeCalledWith(val);
      unmount();
    }
  });
});
