import * as React from "react";
import {
  render,
  fireEvent,
  cleanup,
  getByRole,
  getByTestId,
  getByText,
} from "@testing-library/react";

import {
  PlatformCard,
  PlatformEnum,
  Platform,
} from "../Components/PlatformCard";

type PlatformEnumKey = keyof typeof PlatformEnum;
const platforms: Platform[] = [
  {
    name: "Linux 2 Image",
    description:
      "Linux 2 comes with 5 years of support. It provides Linux kernel 4.14 tuned for optimal performance",
    price: 243.61,
  },
  {
    name: "Ubuntu Server 18.04 LTS",
    description:
      "Linux 2 comes with 5 years of support. It provides Linux kernel 4.14 tuned for optimal performance",
    price: 243.61,
  },
  {
    name: "Red Hat Enterprise Linux 8",
    description:
      "Linux 2 comes with 5 years of support. It provides Linux kernel 4.14 tuned for optimal performance",
    price: 300,
  },
  {
    name: "Microsoft Windows Server 2019 Base",
    description:
      "Linux 2 comes with 5 years of support. It provides Linux kernel 4.14 tuned for optimal performance",
    price: 338.77,
  },
  {
    name: "SUSE Linux Enterprise Server",
    description:
      "Linux 2 comes with 5 years of support. It provides Linux kernel 4.14 tuned for optimal performance",
    price: 200.22,
  },
];

describe("testing platform card component", () => {
  test("if windows platform card renders arm option", () => {
    const { queryByText } = render(
      <PlatformCard
        platformType={PlatformEnum.MICROSOFT_WINDOWS_2019}
        selectPlatform={() => {}}
      />
    );
    const armLabel = queryByText("64-bit (ARM)");

    expect(armLabel).toBeNull;
  });

  test("if non-windows platform card renders arm option", () => {
    type PlatformEnumKey = keyof typeof PlatformEnum;
    for (const plat in Object.keys(PlatformEnum).filter(
      (key) => !isNaN(Number(key))
    )) {
      if (Number(plat) == PlatformEnum.MICROSOFT_WINDOWS_2019) {
        continue;
      }

      const { queryByText, unmount } = render(
        <PlatformCard platformType={Number(plat)} selectPlatform={() => {}} />
      );
      const armLabel = queryByText("64-bit (ARM)");

      expect(armLabel).toBeInTheDocument();
      unmount();
    }
  });

  test("if all platform card renders x86 option", () => {
    for (const plat in Object.keys(PlatformEnum).filter(
      (key) => !isNaN(Number(key))
    )) {
      const { queryByText, unmount } = render(
        <PlatformCard platformType={Number(plat)} selectPlatform={() => {}} />
      );
      const x86Label = queryByText("64-bit (x86)");

      expect(x86Label).toBeInTheDocument();
      unmount();
    }
  });

  test("if appropriate platform and arch is acquistioned on selection", () => {
    for (const plat in Object.keys(PlatformEnum).filter(
      (key) => !isNaN(Number(key))
    )) {
      const onSubmit = jest.fn();
      const { getByText, getByLabelText, unmount } = render(
        <PlatformCard platformType={Number(plat)} selectPlatform={onSubmit} />
      );
      const x86Label = getByLabelText("64-bit (x86)");

      fireEvent.click(x86Label);
      fireEvent.submit(x86Label);
      expect(onSubmit).toBeCalled();
      const indx = Number(plat);
      expect(onSubmit).toBeCalledWith(
        Number(plat),
        "x86",
        platforms[indx].price
      );
      unmount();
    }

    for (const plat in Object.keys(PlatformEnum).filter(
      (key) => !isNaN(Number(key))
    )) {
      if (Number(plat) == PlatformEnum.MICROSOFT_WINDOWS_2019) {
        continue;
      }

      const onSubmit = jest.fn();
      const { getByText, getByLabelText, unmount } = render(
        <PlatformCard platformType={Number(plat)} selectPlatform={onSubmit} />
      );
      const armLabel = getByLabelText("64-bit (ARM)");

      fireEvent.click(armLabel);
      fireEvent.submit(armLabel);
      expect(onSubmit).toBeCalled();
      const indx = Number(plat);
      expect(onSubmit).toBeCalledWith(
        Number(plat),
        "ARM",
        platforms[indx].price
      );
      unmount();
    }
  });
});
